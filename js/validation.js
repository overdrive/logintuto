function emailValidation(email)
{
    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,3})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
}


function passwordValidation(password)
{
    let num  = /([0-9])/;
    let uperCase = /([A-Z])/;
    let lowerCase=/([a-z])/;
    let characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
    let regex = password.match(num) && password.match(lowerCase) && password.match(uperCase)  && password.match(characters);

    if(!regex)
    {
        return false;
    }else{
        return true;
    }
}


function passwordCheck()
{
    let num  = /([0-9])/;
    let uperCase = /([A-Z])/;
    let lowerCase=/([a-z])/;
    let characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
    let password = $('#pass1').val();
    let regex = password.match(num) && password.match(lowerCase) && password.match(uperCase)  && password.match(characters);
    if(password.length < 6)
    {   
        $('#password-strength').removeClass();
        $('#password-strength').addClass('weak-password');
        $('#password-strength').html("Very Weak password (should be atleast 6 characters.)");
    }
    else
    {   
        if(regex)
        {   
            $('#password-strength').removeClass();
            $('#password-strength').addClass('strong-password');
            $('#password-strength').html("Strong");
        }
        else
        { 
                $('#password-strength').removeClass();
                $('#password-strength').addClass('medium-password');
                $('#password-strength').html("Medium (should include alphabets, numbers and special characters.)");
            
            
        }
    }

}

