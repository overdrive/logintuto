<?php
$email =  $_GET['email'];
$token = $_GET['token'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/login.css">

    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jqueryDev.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/validation.js"></script>
    <title>New Password</title>
</head>
<body>
<div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-3">
                <h2 class="login-header">Reset Password</h2>
                <form class="form-container">
                    <div id="message">
                        <!-- <span >All fields are required</span> -->
                    </div>
                      <label for="exampleInputEmail1">New password</label>
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control" id="pass1" placeholder="********" onKeyUp="passwordCheck();">
                      </div>

                      <label for="exampleInputEmail1">Confirm password</label>
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" id="pass2" placeholder="********">
                      </div>
    
                    <input type="button" class="btn btn-primary btn-block" value="Reset" id="reset-button">
                  </form>
    
            </div>  
        </div>
    </div>
<script>
 $(document).ready(function (){
  $('#reset-button').on('click', function(){
        let email = '<?php echo $email; ?>';
        let token = '<?php echo $token; ?>';
        let password1 = $('#pass1').val();
        let password2 = $('#pass2').val();
        if(password1 ==''|| password2 =='')
        {
                $('#message').addClass("alert alert-danger").html("All fields are required");
        }
        else 
        {       if(password1 !== password2)
                {
                        $('#message').addClass("alert alert-danger").html('passwords should be the same!');
                }
                else if(passwordValidation(password1) == false)
                {
                        $('#password1').css('border','1px solid red');
                        $('#password2').css('border','1px solid red');
                        $('#message').addClass("alert alert-danger").html("Make sure your password is strong");
                        //setTimeout(function(){ $('#message').fadeOut(); },5000);
                }
                else
                {
                $.ajax(
                {
                        url: './php/reset_pass.php',  
                        dataType: 'text',
                        method: 'POST' ,
                        data:{  email: email, token:token, passWord: password1 },
                        success: function(response)
                        {
                        if(response.indexOf(1))
                                {
                                        $('#message').html('Your password has been updated');
                                        setTimeout(function(){ $('#message').fadeOut(); },3000);
                                        window.open('index.php');
                                }
                                else
                                {
                                        $('#message').fadeIn("slow").html(" Password or Username is incorrect <br> insert new value and try again ");
                                        $('#password1').val('');
                                        $('#password2').val('');
                                        setTimeout(function(){ $('#message').fadeOut(); },5000);
                                }    
                        }
                                        
                }
                                        
                 );

                        }
                
        }
      
                 });
                })
        
</script>
</body>
</html>