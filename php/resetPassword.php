
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' 
    integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' 
    crossorigin='anonymous'>
    <script src='https://code.jquery.com/jquery-3.4.1.min.js'
    integrity='sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo='
    crossorigin='anonymous'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' 
    integrity='sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6' 
    crossorigin='anonymous'></script>
    <title>Reset password</title>
</head>
<body>
    <div class='container'>
        <span id='error'></span>
    <div >
        <form method='POST' action= 'email.php'>
        <input id='email' type='email' placeholder='Your email' class='input'><br>
        <input type='button' value= 'Reset password' id='forgot-password'>
        <span id= 'msg'></span>
        </form>
    </div>   
    </div>
    <script>

       $(document).ready(function ()
       {
        $('#forgot-password').on('click', function()
         {
             let email = $('#email').val();
             if(email != '')
             {
                $('#email').css('border','1px solid green');
                $.ajax(
                 {  
                    method: 'POST',
                    url: 'email.php',  // the url where am sedding the request to
                    data:{email: email},
                    success: function(response)
                    {
                      if(response == 'success')
                      {
                        alert('success');
                      }
                      else 
                      {
                        alert('No user is registered with this email address!');
                      }
                    }

                        });
                    }
                    else
                    {
                        $('#email').css('border','1px solid red');
                        $('#error').html(' Enter your email');
                    }
            });
        
       });
    </script>
</body>
</html> 







