<?php
include 'db.php';
include 'functions.php';

session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['email']))
{
    $email =$_POST['email'];
    #print_r($email) ;

    $sql = " SELECT * FROM user WHERE email = '$email' ";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) > 0)
    {
       
       $token = bin2hex(random_bytes(20));

       $sql = "UPDATE user SET token='$token', tokenExpire=DATE_ADD(NOW(), INTERVAL 20 MINUTE) WHERE email='$email' ";
       $result = mysqli_query($conn, $sql);

       echo 'success';

       require 'vendor/autoload.php';
       require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
       require 'vendor/phpmailer/phpmailer/src/SMTP.php';

       $mail = new PHPMailer();

       try 
       {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                           
            $mail->isSMTP();                                                  
            $mail->Host       = 'smtp.gmail.com';                           
            $mail->SMTPAuth   = true;                                         
            $mail->Username   = 'consolmwabana@gmail.com';                     
            $mail->Password   = 'consolMwabana@!';                               
            $mail->SMTPSecure = 'tls';                                     
            $mail->Port       = 587;                                    

            //Recipients
            $mail->setFrom('consolmwabana@gmail.com', 'Consolate');
            $mail->addAddress($email);     
                    
            // Content
            $mail->isHTML(true);                                 
            $mail->Subject = 'Reset Password';
            $mail->Body = "
            Hi,<br><br>
            
            In order to reset your password, please click on the link below:<br>
            <a href='
            http://localhost/logintuto/newPassword.php?email=$email&token=$token'>Click here</a><br><br>
            
            Kind Regards,<br>
            Consolate";
            $mail->send();
            echo 'Message has been sent';
            echo '<p> Check your inbox an email has been sent </p>';
    }
    catch (Exception $e) 
        {
          echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    else
    {
        echo 'user not found';
    }
}
else
{
    redirectToLoginPage();
}

?>