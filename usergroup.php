<?php

include 'php/db.php';
include 'dashboard.php';


$statut=false;
$sql = "SELECT * FROM tblusergroup";
$result = mysqli_query($conn, $sql);
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='./css/tables.css' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script> 
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" 
    crossorigin="anonymous"></script>
    <title>User Group</title>
</head>
<body>
    <main>
        <div>
            <form method='POST'>
            <label>Create User group</label>
            <input type="text" placeholder="User group name" id="usergroup" class='field'>
            <input type='button' name='button' id='addGroup' value='Add'>
            </form>
        </div>
    <table class='content-table'>
    <thead>
            <tr>
                <th>ID</th>
                <th>Group name</th>
                <th>Action</th>
            </tr>
        </thead>  
        <tbody>
                <?php
                while($row = mysqli_fetch_assoc($result))
                {
                    ?>
                    <tr>
                    <td><?php echo  $row['GroupID']; ?></td>
                    <td><input type='text'name='groupname' value='<?php  echo  $row['strName'];?>' id='groupName_<?php echo  $row['GroupID']; ?>' class='edit'></td>
                    <td><input type='button'name='button' id='apply_<?php echo  $row['GroupID']; ?>' onclick="updateUser(<?php echo  $row['GroupID']; ?>);" value='edit'></td>
                    </tr>
                   <?php
                }                  
                   ?>     
        </tbody>
        </main>
        <script>
        $(document).ready(function (){
     
            $('#addGroup').on('click',function(){
                let newGroup=$('#usergroup').val();
                
                if( newGroup == '')
                { 
                    $('#usergroup').css('border','1px solid red');
                }
                else 
                {
                    $.ajax
                    ({
                        method: 'POST',
                        url: './php/usergroups.php',  // the url where am sedding the request to
                        data:{newGroup: newGroup},
                        success: function(response)
                        {
                        if(response == 1)
                        {
                            alert('success');
                        }
                        else
                        {
                            alert('Oops,something wrong happened');
                        }
                      
                        }

                            });
                        
                }
               
            });
        
        });
        function updateUser(rowID)
            {
                let strGroupName = $('#groupName_'+rowID).val();
                console.log(strGroupName);
                // let blnRead = 0;
                // let blnWrite = 0;
                // let blnNoAccess = 0;
                // if ($('#blnRead_'+ rowID).is(":checked"))
                //     {
                //         blnRead =1;
                //     }
                //     else if ($('#blnWrite_'+ rowID).is(":checked")){
                //         blnWrite = 1;
                //         console.log('checked');
                //     }
                //     else if($('#blnNoAccess_'+ rowID).is(":checked")) {
                //         blnNoAccess = 1;
                //         console.log('checked');
                //     }
                $.ajax
                    ({
                        method: 'POST',
                        url: './php/usergroups.php', 
                        data:{
                            GroupID: rowID,
                            blnUpdate: 1,
                            strName: strGroupName },
                        success: function(response)
                        {
                        if(response == 1)
                        {
                            alert('success');
                        }
                        else
                        {
                            alert('Oops,something wrong happened');
                        }
                        }
                    });
                           
            }
    
</script>
    </table>
</body>
</html>