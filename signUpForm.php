<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/login.css">

    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jqueryDev.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/validation.js"></script>
    <title>Sign Up</title>
</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-3">
                <h2 class="login-header">Sign Up</h2>
                <form class="form-container">
                    <div id="message">
                        <!-- <span >All fields are required</span> -->
                    </div>
                      <label for="exampleInputEmail1">Name</label>
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" id="username" placeholder="name">
                      </div>
                      
                    <label for="exampleInputEmail1">Email</label>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input type="email" class="form-control" id="email" placeholder="name@example.com">
                      </div>

                      <label for="exampleInputEmail1">Password</label>
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control" id="pass1" placeholder="********" onKeyUp="passwordCheck();">
                      </div>

                      <label for="exampleInputEmail1">Confirm password</label>
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" id="pass2" placeholder="********">
                      </div>
    
                    <input type="button" class="btn btn-primary btn-block" value="Sign Up" id="signup">
                    <small class="row justify-content-center">I am a user<a href="index.php" target="_self"> Sing In</a></small>
                  </form>
    
            </div>  
        </div>
    </div>
<script>

        $(document).ready(function (){
     
            $('#signup').on('click',function(){
                
                let email =$('#email').val();
                let username =$('#username').val();
                let passOne =$('#pass1').val();
                let passTwo =$('#pass2').val();
                
                if(username == ''|| passOne=='' || passTwo=='' || email == '')
                { 
                    $('#message').addClass("alert alert-danger").html("All fields are required");
                    $('#email').css('border','1px solid red');
                    $('#username').css('border','1px solid red');
                    $('#pass1').css('border','1px solid red');
                    $('#pass2').css('border','1px solid red');
                }
                else 
                { 
                    if(emailValidation(email) == false)
                    {   
                        $('#email').css('border','1px solid red');
                        $('#message').addClass("alert alert-danger").html('Invalid email address');
                    }
                    else if(passOne != passTwo)
                    {   
                        $('#pass2').css('border','1px solid red');
                        $('#message').addClass("alert alert-danger").html("Passwords should be the same");
                    }
                    else if(passwordValidation(passOne) == false)
                    {
                        $('#pass1').css('border','1px solid red');
                        $('#pass2').css('border','1px solid red');
                        $('#message').addClass("alert alert-danger").html("Make sure your password is strong");
                    }
                    else{

                    $.ajax
                    ({
                        method: 'POST',
                        url: './php/signUp.php',  // the url where am sedding the request to
                        data:{
                            userName: username,
                            email: email,
                            passWord: passOne
                        },
                        success: function(response)
                        {
                        if(response == 1)
                        {
                            alert('This user as already an account');
                        }
                        else
                        {
                            $('#message').addClass("alert alert-success").html("Your account has been created");
                        }
                      
                        }
                    });}
                }
               
            });
        });

</script>
</body>
</html>