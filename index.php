<?php

session_start();

if(isset($_SESSION['username']))
{
    header('Location: dashboard.php');
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/login.css">

    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jqueryDev.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
      
    <title>Login screen</title>
</head>
<body>
<div class="container-fluid">
        <div class="row justify-content-center">
        <div class="col-12 col-sm-6 col-md-3">
            <h2 class="login-header">Log In</h2>
            <form class="form-container">
                    <div id="error_message">
                        <!-- <span >All fields are required</span> -->
                    </div>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                    </div>
                    <input type="email" class="form-control" id="username" placeholder="username">
                  </div>

                <div class="form-group">
                  <div class="input-group ">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-lock"></i></span>
                    </div>
                    <input type="password" class="form-control" id="password" placeholder="password">
                  </div>
                  
                  <a href="recover.php">Forgot password ?</a>
                </div>

                <input type="button" class="btn btn-primary btn-block" value="Login" id="login">
                <small class="row justify-content-center">Not a user?<a href="signUpForm.php" target="_self">Sign Up</a></small>
              </form>

        </div>
    </div>
    </div>
    <script>
        $(document).ready(function()
        {
            $('#login').on('click', function()
            {
            let username = $("#username").val();
            let pass = $("#password").val();

            if(username === '' || pass === ''){
                
                
                $('#error_message').addClass("alert alert-danger").fadeIn("slow").html("All fields are required");
                 setTimeout(function(){ 
                     $('#error_message').fadeOut();
                     $("#username").css('border','1px solid red');
                     $("#password").css('border','1px solid red'); },5000);
            }else{
                $('#error_message').html('');
                $.ajax(
                    {
                        url: './php/login.php',  
                        dataType: 'text',
                        method: 'POST' ,
                    data:{                
                            userName: username,
                            passWord: pass
                        },
                    success: function(response)
                    {
                        if(response == 1)
                        {
                            window.open('dashboard.php','_self');
                        }
                        else
                        {
                            $('#error_message').addClass("alert alert-danger").fadeIn("slow").html(" Password or Username is incorrect <br> insert new value and try again ");
                            $('#username').val('');
                            $('#password').val('');
                            setTimeout(function(){ $('#error_message').fadeOut(); },5000);
                        }    
                    }
                
                 }
                 
             );
         }
 });

 });
    </script>      
</body>
</html>