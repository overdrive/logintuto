<?php
include 'dashboard.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/dashboard.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap"
    rel="stylesheet"/>
    <script src='https://code.jquery.com/jquery-3.4.1.min.js'
    integrity='sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo='
    crossorigin='anonymous'></script>
    <script src="./js/validation.js" type="text/javascript"></script>
    <title>Profile</title>
</head>
<body>
  <div class='container'>
    <form method='POST' action='signUp.php'>
      <div class="details">
      <h3 style="color: black;">Update details</h3>
      <span class='error_message'></span>
        <input type='text' name='newUsername' id='newUsername' placeholder="New username"><br>
        <input type='email' name='email' id='email' placeholder="email address"><br>
        <input type='button' name='button' id='updDetails' value='Update details'>
      </div> 
      <div class="password">
        <h3>Change your password</h3>
        <span class='wrong_password'></span>
        <input type='password' name='oldpwrd' id="oldpwrd" placeholder=" Old password"><br>
        <input type='password' name='newpwrd1' id="pass1"  onkeyup="passwordCheck();" placeholder="New password"><span id="password-strength"></span><br>
        <input type='password' name='newpwrd1' id="pass2" placeholder=" Confirm password"><br>
        <input type='button' name='button' id='changepwrd' value='Change password'>
      </div>
    </form>
</div>
<script>
        $(document).ready(function (){
     
            $('#updDetails').on('click',function(){
                let newUsername=$('#newUsername').val();
                let email =$('#email').val();
                
                if( email == '' || newUsername== '')
                { 
                    $('.error_message').html("All fields are required");
                    $('#email').css('border','1px solid red');
                    $('#username').css('border','1px solid red');
                }
                else 
                { 
                    if(emailValidation(email) == false)
                    {   
                        $('#email').css('border','1px solid red');
                        $('#error_message').html('Invalid email address');
                    }
                   
                    else{

                    $.ajax
                    ({
                        method: 'POST',
                        url: './php/profile.php',  // the url where am sedding the request to
                        data:{
                            newUserName: newUsername,
                            email: email
                        },
                        success: function(response)
                        {
                        if(response == 1)
                        {
                            alert('success');
                        }
                        else
                        {
                            alert('Oops,something wrong happened');
                        }
                      
                        }

                            });
                        }
                }
               
            });
        $('#changepwrd').on('click',function(){
                let oldPassword =$('#oldpwrd').val();
                let passOne =$('#pass1').val();
                let passTwo =$('#pass2').val();

                if(oldPassword == '' || passOne == '' || passTwo =='')
                { 
                    $('.error_message').html("All fields are required");
                    $('#oldpwrd').css('border','1px solid red');
                    $('#newpwrd1').css('border','1px solid red');
                    $('#newpwrd2').css('border','1px solid red');
                } 
                else
                {
                     if(passOne != passTwo)
                    {   
                        $('#newpwrd1').css('border','1px solid red');
                        $('#newpwrd2').css('border','1px solid red'); 
                        $('.error_message').html("Passwords should be the same");
                    }
                    else if(passwordValidation(passOne) == false)
                    {
                        $('#pass1').css('border','1px solid red');
                        $('#pass2').css('border','1px solid red');
                        $('#error_message').html("Make sure your password is strong");
                    }
                    else
                    {
                      $.ajax
                    ({
                        method: 'POST',
                        url: './php/profile.php',  // the url where am sedding the request to
                        data:{
                            oldPassWord: oldPassword,
                            NewPassWord: passOne
                        },
                        success: function(response)
                        {
                        if(response == 1)
                        {
                            alert('This user as already an account');
                        }
                        else
                        {
                            alert('success');
                        }
                      
                        }

                            });

                    }
                }


        });
        });

</script>
</body>
</html>