<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/dashbaord.css">

    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <title>Document</title>
</head>
<body>
    <div >
        <nav class="navbar navbar-light bg-light">
            <a href="#"  class="btn btn-light p-2" id="menu-toggle"><i class="fas fa-bars"></i></a>
            <a class="navbar-brand btn btn-light"><i class="fas fa-sign-out-alt rotate-icon"></i>Logout</a>
        </nav>
        <div id="wrapper">
            <ul class="sidebar-nav">
                <li class="logo"><a href="#" class="nav-link-logo" ></li>
                <li><a href="#">Home</a></li>
                <li><a href="#"><i class="fas fa-user"></i>Profile</a></li>
                <li><a href="#" data-toggle="collapse" data-target="#submenu"><i class="fas fa-cogs"></i>settings<i class="fas fa-angle-down rotate-icon"></i></a></li>
                    <ul id="submenu" class="collapse">
                        <li><a href=""><i class="fas fa-users"></i>Users</a></li>
                    </ul>
                <li><a href="#"><i class="fas fa-info-circle"></i>Help</a></li>
                <li><a href="#"><i class="fas fa-bell"></i>Notifications</a></li>
            </ul>
        </div>
    </div>
   
</body>
<script>
    $('#menu-toggle').click(function(e){
    e.preventDefault();
    $('#wrapper').toggleClass('menudisplayed');
    });
</script>
</html>