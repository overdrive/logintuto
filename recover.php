
<!DOCTYPE html>
<html lang='en'>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/login.css">

    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jqueryDev.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/validation.js"></script>
    <title>Reset password</title>
</head>
<body>
<div class="container-fluid">
        <div class="row justify-content-center">
        <div class="col-12 col-sm-6 col-md-3">
            <h2 class="login-header">Recover password</h2>
            <form class="form-container">
                    <div id="message">
                        <!-- <span >All fields are required</span> -->
                    </div>
                    <label for="email">Name</label>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                    </div>
                    <input type="email" class="form-control" id="email" placeholder="email">
                  </div>

                <input type="button" class="btn btn-primary btn-block" value="Recover" id="forgot-password">
              </form>

        </div>
    </div>
    </div>
<script>

$(document).ready(function ()
  {
    $('#forgot-password').on('click', function()
        {
          let email = $('#email').val();
          if(email != '')
             {
               if(emailValidation(email)== false)
               {
                $('#email').css('border','1px solid red');
                $('#message').addClass("alert alert-danger").html('Invalid email');
               }else
               {
                $('#email').css('border','1px solid green');
                $.ajax(
                 {  
                    method: 'POST',
                    url: './php/email.php',  
                    data:{email: email},
                    success: function(response)
                    {
                      if(response.indexOf('success'))
                      {
                       $('#message').addClass("alert alert-success").html('An email has been sent check your inbox!');
                        $('#email').val('');
                      }
                      else 
                      {
                        $('#message').addClass("alert alert-danger").html('No user is registered with this email address!');
                      }
                    }

                        });

               }

                
               }
                    else
                    {
                        $('#email').css('border','1px solid red');
                        $('#error').html(' Enter your email');
                    }
            });
        // cancel button
        $('#cancel').on('click', function(){
          window.open('index.php');
        });
       });
    </script>
</body>
</html> 







